<?php

declare(strict_types=1);

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Pimple\Container;
use Laminas\Twitter\Twitter;
use SlideBotAPI\Actions\HomeAction;
use SlideBotAPI\Actions\HelloWorldAction;

return function (Container $container) {
  /*
   * Special case, I want to be able to get 'logger'.
   */
    $container['logger'] = function ($c) {
        // monolog container
        $settings = $c['settings']['logger'];
        $logger = new Logger($settings['name']);
        $logger->pushHandler(
            new StreamHandler($settings['path'], $settings['level'])
        );
        return $logger;
    };

    $container[Twitter::class] = function ($c) {
        return new Twitter();
    };

};
