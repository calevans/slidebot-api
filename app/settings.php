<?php

/**
 * This returns a closure that itself returns an array when called with
 * the proper parameters
 *
 * NO KEYS OT SECRETS ARE TO BE STORED IN THIS FILE. USE AN .env FILE FOR THAT.
 */

declare(strict_types=1);

use Pimple\Container;
use Monolog\Logger;

return function (string $appRoot, bool $development = true) {
    return [
        'development' => $development,
        'displayErrorDetails' => $development,
        'logger' => [
            'name' => 'cardbuilder',
            'path' => $appRoot . 'logs/app.log', // change to /var/log at some point.
            'level' => Logger::DEBUG,
            ],
        'paths' => [
            'root' => $appRoot,
            'data' => $appRoot . 'data/',
            'logs' => $appRoot . 'logs/'
        ],
        'twitter' => [
          'phparty' => [
            'name' => 'phparty',
            'access_token' => [
              'token' => '<token value>',
              'secret' => '<token secret value>',
            ],
            'oauth_options' => [
                'consumerKey' => '<consumer key value>',
                'consumerSecret' => '<consumer secret value>',
            ]
          ],
          'calevans' => [
            'name' => 'calevans',
            'access_token' => [
              'token' => '<token value>',
              'secret' => '<token secret value>',
            ],
            'oauth_options' => [
                'consumerKey' => '<consumer key value>',
                'consumerSecret' => '<consumer secret value>',
            ]
          ]
        ]

    ];
};
