<?php

declare(strict_types=1);

use Pimple\Container;
use Pimple\Psr11\Container as Psr11Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use SlideBotAPI\Actions\HelloWorldAction;

require __DIR__ . '/vendor/autoload.php';

$appRoot = __DIR__ . '/';
$development = true;

// Read settings and create Pimple container
$settings = require $appRoot . 'app/settings.php';
$container = new Container(['settings' => $settings($appRoot, $development)]);

// Set up dependencies
$dependencies = require $appRoot . 'app/dependencies.php';
$dependencies($container);

// Instantiate App
$app = AppFactory::create(null, new Psr11Container($container));

/*
 * Add App level middleware
 */
$app->addErrorMiddleware(true, true, true);

/*
 * Define Routes
 */
$app->get(
    '/api/slides/{slideNum}',
    function (Request $request, Response $response, array $args) {
        $this->get('logger')->info('Hello World!');
        $this->get('logger')->info('Slide #' . $args['slideNum']);
        return $response->withStatus(200);
    }
);

/*
 * For testing only
 */
$app->get('/api/hello', HelloWorldAction::class);

/*
 * Catch everything else
 */
$app->get('/api/', HomePageHandler::class);




$app->get(
    '/api/init/',
    function (Request $request, Response $response, array $args) {
    }
);
// Main Routes
/**
 * Init a new session. This is where we take over the account.
 * - Change name
 * - Change picture
 * - initial tweet
 */
// OLD CODE NOT YET VETTED
// OLD CODE NOT YET VETTED
// OLD CODE NOT YET VETTED
/*
$app->get('/api/init/',
  function (Request $request, Response $response, array $args) {
      //
      // Sending tweets needs to go in a tweet domain
      // modifying the twitter account needs to go in an Account domain
      //
  $twitter_config = include $this->slideBot['config_dir'] . 'twitter.php';
  $twitter = new Twitter($twitter_config['twitter']);
  $twitter_response = $twitter->account->verifyCredentials();
  $payload = include $this->slideBot['config_dir'] . 'new_profile.php';
  $twitter_response_object = $twitter_response->toValue();
  file_put_contents(
    $this->slideBot['config_dir'] . 'orig_profile.json',
    json_encode($twitter_response_object,JSON_PRETTY_PRINT)
  );

  $image_url = $twitter_response_object->profile_image_url;
  $url_parts = pathinfo($image_url);
  $image_basename = $url_parts['dirname'] .
    '/' .
    (explode('_',$url_parts['filename'])[0]) .
    '_400x400.'.
    $url_parts['extension'];
  file_put_contents(
    $this->slideBot['config_dir'] . 'orig_image',
    file_get_contents($image_basename)
  );

  $twitter_response = $twitter->account->updateProfile($payload);

  if (file_exists($this->slideBot['config_dir'] . 'new_image') ) {
    $img = base64_encode(file_get_contents($this->slideBot['config_dir'] . 'new_image'));
    $twitter->account->accountUpdateProfileImage(['image'=>$img]);
  }

  return $response->withJson(['response'=>$twitter_response->toValue()],200 );

  }
);
*/
/**
 * Called upon completion.
 *
 * - Goodbye tweet
 * - Restore the original image
 */
// OLD CODE NOT YET VETTED
// OLD CODE NOT YET VETTED
// OLD CODE NOT YET VETTED
$app->get(
    '/reset/',
    function (Request $request, Response $response, array $args) {
        $twitter_config = include $this->slideBot['config_dir'] . 'twitter.php';
        $twitter = new Twitter($twitter_config['twitter']);
        $twitter_response = $twitter->account->verifyCredentials();
        $orig_account = json_decode(file_get_contents($this->slideBot['config_dir'] . 'orig_profile.json'), true);
        $payload = [
        'name' => $orig_account['name'],
        'screen_name' => $orig_account['screen_name'],
        'description' => $orig_account['description'],
        'location' => $orig_account['location'] ?? '',
        ];
        $twitter_response = $twitter->account->updateProfile($payload);
    // CHECK FOR TWITTER ERRORS HERE.
        if (file_exists($this->slideBot['config_dir'] . 'orig_image')) {
            $img = base64_encode(file_get_contents($this->slideBot['config_dir'] . 'orig_image'));
            $twitter->account->accountUpdateProfileImage(['image' => $img]);
            unlink($this->slideBot['config_dir'] . 'orig_image');
        }
        return $response->withJson(['response' => $twitter_response->toValue()], 200);
    }
);

/**
 * Main processing.
 * Call this on slide change with the slide number. If there is an unsent tweet
 * for this slide then send it.
 */
// OLD CODE NOT YET VETTED
// OLD CODE NOT YET VETTED
// OLD CODE NOT YET VETTED
/*
$app->get('/slides/[{slide_no}]',
  function (Request $request, Response $response, array $args) {
  $file_name = $this->slideBot['slide_dir'] . $args['slide_no'] . '.tweet';

  if (! file_exists($file_name)) {
    return $response->withStatus(404);
  }

  $twitter_config = include $this->slideBot['config_dir'] . 'twitter.php';
  $twitter = new Twitter($twitter_config['twitter']);
  $payload = file_get_contents($file_name);
  $twitter_response = $twitter->statuses->Update($payload . time());

  $new_file_name = $this->slideBot['slide_dir'] . $args['slide_no'] . '.used';
  rename($file_name,$new_file_name);


  return $response->withStatus(200);
});
*/
$app->run();
