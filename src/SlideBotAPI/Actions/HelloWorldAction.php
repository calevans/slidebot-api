<?php

/**
 * This is just a baseline Action
 *
 * it is currently working.
 */

declare(strict_types=1);

namespace SlidebotAPI\Actions;

use Pimple\Container;
use Pimple\Psr11\Container as Psr11Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class HelloWorldAction
{
    protected $container = null;

    public function __construct(Psr11Container $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response)
    {
        $this->container->get('logger')->info('Hello World!');
        echo "Hello World!";
        return $response;
    }
}
