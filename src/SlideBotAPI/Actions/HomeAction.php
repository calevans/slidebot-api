<?php

declare(strict_types=1);

namespace SlidebotAPI\Actions;

use Pimple\Container;
use Pimple\Psr11\Container as Psr11Container;

class HomeAction
{
    protected $container = null;

    public function __construct(Psr11Container $container)
    {
        $this->container = $container;
    }

    public function handle(Request $request, Response $response)
    {
        throw new \Exception('Invalid Route', 404);
        return $response;
    }
}
