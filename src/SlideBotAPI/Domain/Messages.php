<?php

/**
 * Initalized with a slide deck name, this manages the messages for that deck.
 */

declare(strict_types=1);

namespace SlideBotAPI\Domain;
