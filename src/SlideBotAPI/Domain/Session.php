<?php

/**
 * This needs to be initalized with a slide deck name.
 * It manages the session.
 * This controlls INIT and RESET actions.
 * Record start time. Auto RESET if elapsed time is more than 4 hours.
 */

declare(strict_types=1);

namespace SlidebotAPI\Domain;
