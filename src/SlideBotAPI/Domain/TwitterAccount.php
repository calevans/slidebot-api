<?php

/**
 * This represents the twiter account. it stores the original information and
 * handles setting and resetting name and image
 */

declare(strict_types=1);

namespace SlidebotAPI\Domain;
