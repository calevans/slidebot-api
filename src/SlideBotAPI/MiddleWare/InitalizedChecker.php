<?php

declare(strict_types=1);

namespace Slidebot\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

/**
 * Checks to see if a Slidebot session has been initalized. If not, aborts
 * immediatly.
 */

class InitalizedChecker
{

    public function __invoke(Request $request, RequestHandler $handler): Response
    {
      /*
       * check here for an initalized slide session and if it does not exist then
       * throw an exception.
       */

        return $handler->handle($request);
    }
}
